s = "Today is nice day, so we can go anywhere"
print("A)")
print("The number of characters in the given string: ", len(s)," Characters")

print("----------------------------------------------------------")
print("B)")
print("Positions from -5 to -9 are: ", s[-5:-9])


print("----------------------------------------------------------")
print("C)")
print("The Jump character is 3: ", s[::3])

print("----------------------------------------------------------")
print("D)")
print(s.split(","))

print("----------------------------------------------------------")
print("E)")
s1 = "Today is nice day"
s2 = "So we can go anywhere"
L1 = (s1+" "+ "," +" "+ s2)
print(L1)
L2  = sorted(L1)
print(L2)



print("----------------------------------------------------------")
print("F)")
print(L1.upper())
