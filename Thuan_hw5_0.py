print("Part1")
def hypotenuse(leg1,leg2): #our function has 2 arguments for first leg and second leg respectively
    return (leg1**2+leg2**2)**(1/2) #according to Pythagorean theorem, the hypotenuse is equal to the square root of the sum of the squares of the legs
                                   

  
print("Test number 1, triangle with legs 3 and 4, hypotenuse is:",hypotenuse(3,4))#prints the call for triangle with legs 3 and for
print("Expected 5")#expected result
print("Test number 1, triangle with legs 12 and 5, hypotenuse is:",hypotenuse(12,5))#same for 12 and 5
print("Expected 13")
print("Test number 1, triangle with legs 20 and 21, hypotenuse is:",hypotenuse(20,21))#20 and 29
print("Expected 29")


print("--------------------------")
print("Part2")
#PART 2
def rhombus_area(d1,d2):
    return 1/2*(d1*d2)

print(rhombus_area(2,5))
#5.0
print(rhombus_area(10,5))
#25.0
print(rhombus_area(25,5))
#62.5



#PART 2.1
def divide(x, y): 
    try:
        result = x // y 
        print("Yeah ! Your answer is :", result)
    except ZeroDivisionError: 
        print("Sorry ! You are dividing by zero ")

divide(3,5)
#Yeah ! Your answer is : 0
divide(10,0)
#Sorry ! You are dividing by zero 
divide(5,5)
#Yeah ! Your answer is : 1
