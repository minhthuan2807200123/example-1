#Example 1: Define a function that takes an argument. Call the function. Identify what code is the argument and what code is the parameter.
print("EXAMPLE 1")
def mail(): # Nó sẽ xác định hàm không có giá trị đầu vào 
    print("Hello everyone!!!")

#Gọi lại tên hàm đã thông báo ở trên
mail()

print(" ")
print("EXAMPLE 2")
#Example 2: Call your function from Example 1 three times with different arguments: a value, a variable, and an expression. Identify which kind of argument is which.
def f(n):
    return print(n**4)
f(2) #bước thế 2 vào f(n)
num = 1 #khi này num sẽ khai báo là 1 
f(num) #gọi hàm num bên trên
f(100+8) #ở đây máy sẽ hiểu đây là 1 n: lấy tổng trong f(n) thế vào công thức ở trên (n**4)

print(" ")
print("EXAMPLE 3")
#Example 3: Create a function with a local variable. Show what happens when you try to use that variable outside the function. Explain the results.
def add(n1,n2):
    sumTotal = n1 + n2     #ở đây biến cục bộ sẽ bên trong 1 hàm 
    return sumTotal
sumTotal = 20             #Và biến 20 sẽ nằm bên ngoài hàm
print(f"Total = {sumTotal}")

#Hai hàm này sẽ có 2 biến không chồng chéo lên nhau
def main():
    print(f"8+4 = {add(8,4)}")  #còn ở hàm main() này đã có thêm 2 biến khác với hàm def() là 8 và 4
main()

print(" ")
print("EXAMPLE 4")
#Example 4: Create a function that takes an argument. Give the function parameter a unique name. Show what happens when you try to use that parameter name outside the function. Explain the results.
def T(a):
    print(a)
a = 12 + 9
T(a)  #Nó sẽ in ra kết quả của a trong hàm T là 21

print(" ")
print("EXAMPLE 4")
#Example 5: Show what happens when a variable defined outside a function has the same name as a local variable inside a function. Explain what happens to the value of each variable as the program runs.
def H():
    var = 9    #Ở đây nó sẽ là 1 biến bên trong
    return var
var1 = 12      #var1 và var2 nó sẽ là 2 biến ngoài hàm  
var2 = 2001
print(var1)     #Theo như thứ tự thì máy sẽ in ra kết quả của var1
print(H())      #Đến hàm H()
print(var2)     #Cuối cùng đến var2
