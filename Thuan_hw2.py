#viết 1 chương trình python để yêu cầu người dùng nhập tên và tuổi của họ. Sau đó gửi lại 1 tin nhắn: cho họ biết năm họ tròn 70 tuổi.
#Luu ý: xuất kể CMD,tên file tự đặt
print("EX1")
name = input("Please Enter Yours Name: ")
age = int(input("Please Enter Your AGE: "))
year = str((2021 - age) + 70 )
print(name + "\nwill be 70 years old in the year " + year)

print(" ")
print("EX2")
#Viết 1 chương trình python yêu câu người dung nhập 1 chuỗi bất kỳ bằng tiếng anh
#Hãy xuất ra cmd : 1 chuỗi mới với thứ tự đạo ngược chuỗi ban đầu

n = input("Please Enter String: ")
print(" ".join(reversed(n)))

print(" ")
print("EX3")
#Câu 1: cho 1 cái chuỗi sau : Machine learning for Covid-19 diagnosis
#a) Hãy đếm tổng số ký tự có trong cái chuỗi
#b) kiểm tra trong chuỗi đã cho có ký tự chuỗi số hay không ?
#c) Chuyển chuỗi đã cho thành chuỗi in HOA
#d) thay thế chuỗi đã cho thành 1 chuỗi mới bất kỳ dược nhập từ bàn phím. Xuất ra màn hình CMD : ĐẦU ĐẾN VỊ TRÍ SỐ 4
#e) loại bỏ bất kỳ khoảng trắng từ dầu dòng hoặc cuối dòng trong cái chuỗi đã cho
#f) hãy tách cái chuỗi đã cho được ngăn cách với nhau bằng dấu ,
#g)hãy kiểm tra
print("a)")
s = "Machine learning for Covid-19 diagnosis"
print("Length is: ", len(s), " Characters")

print(" ")
print("b)")
print(s.isdigit())

print(" " )
print("c)")
print(s.upper())

print(" ")
print("d)")
S1 = input("Please Enter new String: ")
print(S1[0:4])

print(" ")
print("e)")
print(S1.replace(" ",""))

print(" ")
print("f)")
s = "Machine learning for Covid-19 diagnosis, by programming code"
print(s.split(","))

print(" ")
print("EX4")
#Câu 2 hãy viết chương trình python thu thập dữ liệu người dùng từ bàn phím : tuổi, quê quán, nơi đang sống
#Sau đó sử dụng phương pháp định dạng chuỗi để tạo thành 1 chuỗi có nghĩa
name = input("Please enter your name: ")
age = input("Please enter your age: ")
address = input("Please enter address: ")
home_country = input("Please enter your home country: ")
print("My name’s {}, I’m {} years old, I’m living in {}, I’m originally from {}. ".format(name,age,address,home_country))
